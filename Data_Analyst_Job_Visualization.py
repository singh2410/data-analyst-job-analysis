#!/usr/bin/env python
# coding: utf-8

# # Data Analyst Job Visualization
# #By- Aarush Kumar
# #Dated: July 05,2021

# In[24]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import plotly.express as px
import plotly.graph_objects as go
import seaborn as sns
from wordcloud import WordCloud


# In[25]:


df=pd.read_csv(r'/home/aarush100616/Downloads/Projects/Data Analyst Job Visualization/DataAnalyst.csv')


# In[26]:


df


# In[27]:


df.head()


# ## Data cleaning

# In[28]:


# Remove unnamed column
df.drop(['Unnamed: 0'], axis=1,inplace=True)


# In[29]:


# Check missing values
def missing_values_table(df):
        mis_val = df.isnull().sum()
        # Percentage of missing values
        mis_val_percent = 100 * df.isnull().sum() / len(df)
       # Make a table with the results
        mis_val_table = pd.concat([mis_val, mis_val_percent], axis=1)
       # Rename the columns
        mis_val_table_ren_columns = mis_val_table.rename(
        columns = {0 : 'Missing Values', 1 : '% of Total Values'})
        # Sort the table by percentage of missing descending
        mis_val_table_ren_columns = mis_val_table_ren_columns[
            mis_val_table_ren_columns.iloc[:,1] != 0].sort_values(
        '% of Total Values', ascending=False).round(1)
       # Print some summary information
        print ("Your selected dataframe has " + str(df.shape[1]) + " columns.\n"      
            "There are " + str(mis_val_table_ren_columns.shape[0]) +
              " columns that have missing values.")
        
        # Return the dataframe with missing information
        return mis_val_table_ren_columns


# In[31]:


missing_values_table(df)


# In[32]:


df['Easy Apply'].value_counts()


# In[33]:


df['Competitors'].value_counts()


# In[34]:


df['Rating'].value_counts()[:5]


# In[35]:


df=df.replace(-1,np.nan)
df=df.replace(-1.0,np.nan)
df=df.replace('-1',np.nan)


# In[36]:


missing_values_table(df)


# In[37]:


df['Company Name'],_=df['Company Name'].str.split('\n', 1).str


# In[38]:


df['Job Title'],df['Department']=df['Job Title'].str.split(',', 1).str


# In[39]:


df['Salary Estimate'],_=df['Salary Estimate'].str.split('(', 1).str


# In[40]:


## Split salary into two columns min salary and max salary
df['Min_Salary'],df['Max_Salary']=df['Salary Estimate'].str.split('-').str
df['Min_Salary']=df['Min_Salary'].str.strip(' ').str.lstrip('$').str.rstrip('K').fillna(0).astype('int')
df['Max_Salary']=df['Max_Salary'].str.strip(' ').str.lstrip('$').str.rstrip('K').fillna(0).astype('int')


# In[41]:


df.drop(['Salary Estimate'],axis=1,inplace=True)


# ## Current openings

# In[42]:


df['Easy Apply']=df['Easy Apply'].fillna(False).astype('bool')


# In[43]:


df_easy_apply=df[df['Easy Apply']==True]
df_1=df_easy_apply.groupby('Company Name')['Easy Apply'].count().reset_index()
company_opening_df=df_1.sort_values('Easy Apply',ascending=False).head(10)


# In[44]:


plt.figure(figsize=(10,5))
chart = sns.barplot(
    data=company_opening_df,
    x='Company Name',
    y='Easy Apply',
    palette='Set1'
)
chart=chart.set_xticklabels(
    chart.get_xticklabels(), 
    rotation=65, 
    horizontalalignment='right',
    fontweight='light',
)


# ### Salary Distribution of Data Analyst

# In[46]:


data_analyst = df[df['Job Title']=='Data Analyst']
sns.set(style="white", palette="muted", color_codes=True)
f, axes = plt.subplots(1, 2, figsize=(15, 8), sharex=True)
sns.despine(left=True)
#Plot a histogram and kernel density estimate
sns.distplot(data_analyst['Min_Salary'], color="b", ax=axes[0])
sns.distplot(data_analyst['Max_Salary'], color="r",ax=axes[1])
plt.setp(axes, yticks=[])
plt.tight_layout()


# ## Top 20 cities with their minimum and maximum salaries

# In[47]:


df_1=df.groupby('Location')[['Max_Salary','Min_Salary']].mean().sort_values(['Max_Salary','Min_Salary'],ascending=False).head(20)


# In[48]:


fig = go.Figure()
fig.add_trace(go.Bar(x=df_1.index,y=df_1['Min_Salary'],name='Minimum salary'))
fig.add_trace(go.Bar(x=df_1.index,y=df_1['Max_Salary'],name='Maximum Salary'))
fig.update_layout(title='Top 20 cities with their minimum and maximum salaries',barmode='stack')
fig.show()


# ## Top 20 Roles with their minimum and maximum salaries

# In[49]:


df_1=df.groupby('Job Title')[['Max_Salary','Min_Salary']].mean().sort_values(['Max_Salary','Min_Salary'],ascending=False).head(20)


# In[50]:


fig = go.Figure()
fig.add_trace(go.Bar(x=df_1.index,y=df_1['Min_Salary'],name='Minimum salary'))
fig.add_trace(go.Bar(x=df_1.index,y=df_1['Max_Salary'],name='Maximum Salary'))
fig.update_layout(title='Top 20 Roles with their minimum and maximum salaries',barmode='stack')
fig.show()


# ## Size of Employees Vs No of Companies

# In[52]:


df_1=df['Size'].value_counts()


# In[53]:


df_1=pd.DataFrame(df_1)
df_1['employee_size']=df_1.index


# In[54]:


df_1.reset_index(inplace=True)
df_1.drop(['index'],axis=1,inplace=True)


# In[55]:


df_1=df_1.rename(columns={"Size": "No_of_companies"})


# In[56]:


plt.figure(figsize=(10,5))
chart = sns.barplot(
    data=df_1,
    x='employee_size',
    y='No_of_companies',
    palette='Set1'
)
chart=chart.set_xticklabels(
    chart.get_xticklabels(), 
    rotation=65, 
    horizontalalignment='right',
    fontweight='light',
)


# ## Revenue of different sectors

# In[57]:


def filter_revenue(x):
    revenue=0
    if(x== 'Unknown / Non-Applicable' or type(x)==float):
        revenue=0
    elif(('million' in x) and ('billion' not in x)):
        maxRev = x.replace('(USD)','').replace("million",'').replace('$','').strip().split('to')
        if('Less than' in maxRev[0]):
            revenue = float(maxRev[0].replace('Less than','').strip())
        else:
            if(len(maxRev)==2):
                revenue = float(maxRev[1])
            elif(len(maxRev)<2):
                revenue = float(maxRev[0])
    elif(('billion'in x)):
        maxRev = x.replace('(USD)','').replace("billion",'').replace('$','').strip().split('to')
        if('+' in maxRev[0]):
            revenue = float(maxRev[0].replace('+','').strip())*1000
        else:
            if(len(maxRev)==2):
                revenue = float(maxRev[1])*1000
            elif(len(maxRev)<2):
                revenue = float(maxRev[0])*1000
    return revenue


# In[58]:


df['Max_revenue']=df['Revenue'].apply(lambda x: filter_revenue(x))


# In[59]:


df_1=df.groupby('Sector')[['Max_revenue']].mean().sort_values(['Max_revenue'],ascending=False).head(20)


# In[60]:


df_1.reset_index(inplace=True)


# In[61]:


df_1


# In[62]:


plt.figure(figsize=(10,5))
chart = sns.barplot(
    data=df_1,
    x='Sector',
    y='Max_revenue'
)
chart.set_xticklabels(
    chart.get_xticklabels(), 
    rotation=65, 
    horizontalalignment='right',
    fontweight='light',
 
)
chart.axes.yaxis.label.set_text("Revenue(Million dollars)")


# ## Revenue of different Industries

# In[63]:


df_1=df.groupby('Industry')[['Max_revenue']].mean().sort_values(['Max_revenue'],ascending=False).head(20)


# In[64]:


df_1.reset_index(inplace=True)


# In[65]:


plt.figure(figsize=(10,5))
chart = sns.barplot(
    data=df_1,
    x='Industry',
    y='Max_revenue',
    palette='Set1'
)
chart.set_xticklabels(
    chart.get_xticklabels(), 
    rotation=65, 
    horizontalalignment='right',
    fontweight='light',
 
)
chart.axes.yaxis.label.set_text("Revenue(Million dollars)")


# ## Word Cloud of job Title

# In[67]:


job_title=df['Job Title'][~pd.isnull(df['Job Title'])]
wordCloud = WordCloud(width=450,height= 300).generate(' '.join(job_title))
plt.figure(figsize=(19,9))
plt.axis('off')
plt.title(df['Job Title'].name,fontsize=20)
plt.imshow(wordCloud)
plt.show()


# ## Ratings

# In[68]:


count=df.groupby('Rating')['Company Name'].count()


# In[69]:


df_1=pd.DataFrame(count).rename(columns={'Company Name': 'Count'}).reset_index()


# In[70]:


df_1=df_1.sort_values('Rating',ascending=False).head(20)


# In[71]:


plt.figure(figsize=(10,5))
chart = sns.barplot(
    data=df_1,
    x='Rating',
    y='Count',
    palette='Set1'
)
chart.set_xticklabels(
    chart.get_xticklabels(), 
    rotation=65, 
    horizontalalignment='right',
    fontweight='light',
 
)
chart.axes.yaxis.label.set_text("No. of companies")


# ## Industries with their Average minimum and maximum salaries

# In[72]:


df_1=df.groupby('Industry')[['Min_Salary','Max_Salary']].mean().rename(columns={'Min_Salary':'Avg_min_salary','Max_Salary':'Avg_max_salary'})


# In[73]:


df_1=df_1.reset_index()


# In[74]:


df_1=df_1.sort_values(['Avg_max_salary','Avg_max_salary'],ascending=False)


# In[75]:


fig = go.Figure()
fig.add_trace(go.Bar(x=df_1.Industry,y=df_1['Avg_min_salary'],name='Average Minimum salary'))
fig.add_trace(go.Bar(x=df_1.Industry,y=df_1['Avg_max_salary'],name='Average Maximum Salary'))
fig.update_layout(title='Industries with their Average minimum and maximum salaries',barmode='stack')
fig.show()


# ## Jobs with Openings

# In[76]:


df_1=pd.DataFrame(df[df['Easy Apply']==True]['Job Title'].value_counts()).rename(columns={'Job Title':'No_of_openings'})


# In[77]:


df_1=df_1.reset_index().rename(columns={'index':'Job Title'})


# In[78]:


df_1=df_1.head(10)


# In[79]:


plt.figure(figsize=(10,5))
chart = sns.barplot(
    data=df_1,
    x='Job Title',
    y='No_of_openings',
    palette='Set1'
)
chart=chart.set_xticklabels(
    chart.get_xticklabels(), 
    rotation=65, 
    horizontalalignment='right',
    fontweight='light',
)

